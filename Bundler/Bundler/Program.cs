﻿using System;
//using Hl7.Fhir.ElementModel;
//using Hl7.Fhir.R4.Core;
using Hl7.Fhir.Model;
//using H7.Fhir.R4.Specification;
using Hl7.Fhir.Serialization;
//using HL7.Fhir.Support;
//using HL7.FhirPath;

using Newtonsoft.Json;

namespace Bundler
{
    public class BCS
    {

    }

    public class RawPatientCache
    {
        public void addRawPatient( string PatientId, BCS bcs )
        {
        }

        public void clearCache()
        {

        }

        public BCS SearchForPatient( string PatientId )
        {
            return new BCS();
        }
    }

    public class Assembler
    {
        static Resource Assemble( string resourceType, string PatientId)
        {
            return new Patient();
        }

        public static Patient assemblePatient()
        {
            Patient patient = new Patient();
            patient.Id = "1";

            HumanName humanName = new HumanName();
            humanName.Family = "Fraker";
            humanName.GivenElement.Add(new FhirString("Becky"));
            patient.Name.Add(humanName);

            patient.BirthDateElement = new Date(1979, 2, 2);
            patient.Gender = AdministrativeGender.Female;

            Identifier identifier = new Identifier();
            identifier.Value = "22222222";
            identifier.System = "NY Medicaid";

            patient.Identifier.Add(identifier);
            return patient;
        }

        public static Condition assembleCondition()
        {
            Condition condition = new Condition();
            condition.Subject = new ResourceReference("Patient/1", "Becky Fraker");

            condition.Code = new CodeableConcept();

            Coding ccCode = new Coding("http://snomed.info/sct", "39065001", "Burn of Ear");
            condition.Code.Coding.Add( ccCode );

            return condition;
        }

        public static Organization assembleOrganization()
        {
            Organization org = new Organization();
            org.Name = "Partner Org 111";
            org.Id = "111";

            Identifier identifier = new Identifier();
            identifier.Value = "111";
            org.Identifier.Add( identifier );

            return org;
        }

        public static Practitioner assemblePractitioner()
        {
            Practitioner practitioner = new Practitioner();
            practitioner.Id = "2222";

            HumanName humanName = new HumanName();
            humanName.Family = "Who";
            humanName.GivenElement.Add(new FhirString("Dr"));

            practitioner.Name.Add( humanName) ;

            return practitioner;
        }

        public static Encounter assembleEncounter()
        {
            Encounter encounter = new Encounter();
            encounter.Id = "11";
            encounter.Subject = new ResourceReference("Patient/1", "Becky Fraker");

            Identifier identifier = new Identifier();
            identifier.Assigner = new ResourceReference("Organization/111");
            identifier.Value = "787878";
            encounter.Identifier.Add(identifier);

            Period period = new Period();
            period.Start = DateTime.Now.ToLongDateString();
            period.End = DateTime.Now.ToLongDateString();
            encounter.Period = period;

            encounter.Status = Encounter.EncounterStatus.Finished;
            CodeableConcept inpatientConcept = new CodeableConcept();

            inpatientConcept.Coding.Add(new Coding("http://terminology.hl7.org/CodeSystem/v3-ActCode", "IMP", "inpatient encounter"));
            encounter.ServiceType = inpatientConcept;

            encounter.Subject = new ResourceReference("Patient/1","Becky Fraker");
            Encounter.ParticipantComponent ecp = new Encounter.ParticipantComponent();
            ecp.Individual = new ResourceReference("Practitioner/2222", "Dr Who");
            encounter.Participant.Add(ecp);

            return encounter;
        }

        internal static Resource assembleMedicationOrder()
        {
            MedicationRequest medicationRequest = new MedicationRequest();
            medicationRequest.Subject = new ResourceReference("Patient/1", "Becky Fraker");

            medicationRequest.Status = MedicationRequest.medicationrequestStatus.Active;

            medicationRequest.Id = "1";

            medicationRequest.Category.Add(new CodeableConcept("http://terminology.hl7.org/CodeSystem/medicationrequest-category", "outpatient", "outpatient")); 

            medicationRequest.Performer = new ResourceReference("Practitioner/2222", "Dr Who");

 //           medicationRequest.Medication. = new Medication();

            medicationRequest.AuthoredOn = "2015-03-01";

            Identifier identifier = new Identifier();
            identifier.Value = "777777";

            return medicationRequest;

        }

        internal static Resource assembleFlag()
        {
            Flag flag = new Flag();
            flag.Subject = new ResourceReference("Patient/1", "Becky Fraker");

            return flag;
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            Bundle bundle = new Bundle();
            bundle.Id = "111111";
            bundle.Type = Bundle.BundleType.Searchset;
            Meta meta = new Meta();
            meta.LastUpdated = new DateTimeOffset(DateTime.Now);
            bundle.Meta = meta; 

            Bundle.EntryComponent bec = new Bundle.EntryComponent();
            bec.Resource = Assembler.assembleOrganization();
            bundle.Entry.Add(bec);

            bec = new Bundle.EntryComponent();
            bec.Resource = Assembler.assemblePractitioner();
            bundle.Entry.Add(bec);

            bec = new Bundle.EntryComponent();
            bec.Resource = Assembler.assemblePatient();
            bundle.Entry.Add(bec);

            bec = new Bundle.EntryComponent();
            bec.Resource = Assembler.assembleEncounter();
            bundle.Entry.Add(bec);

            bec = new Bundle.EntryComponent();
            bec.Resource = Assembler.assembleCondition();
            bundle.Entry.Add(bec);

            bec = new Bundle.EntryComponent();
            bec.Resource = Assembler.assembleMedicationOrder();
            bundle.Entry.Add(bec);

            bec = new Bundle.EntryComponent();
            bec.Resource = Assembler.assembleFlag();
            bundle.Entry.Add(bec);

            FhirXmlSerializer FhirXmlSerializer = new FhirXmlSerializer();

            Console.WriteLine(FhirXmlSerializer.SerializeToString(bundle, Hl7.Fhir.Rest.SummaryType.False));
        }
    }
}
 